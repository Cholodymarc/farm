package farm;

import java.util.ArrayList;
import java.util.List;

public class Granary {
    private List<Product> products;

    Granary() {
        this.products = new ArrayList<>();
    }

    public List<Product> getProducts() {
        return products;
    }

    public Product getProductByName(String name){
        for(Product product : products){
            if(product.getName().equalsIgnoreCase(name)){
                return product;
            }
        }
        return null;
    }
}
