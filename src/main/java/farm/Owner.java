package farm;

public class Owner implements IOwner {
    private double cashAmount;

    Owner(double cashAmount) {
        this.cashAmount = cashAmount;
    }

    public double getCashAmount() {
        return cashAmount;
    }

    public void increaseCashAmount(double amount) {
        this.cashAmount += amount;
    }

    public void decreaseCashAmount(double amount) {
        this.cashAmount -= amount;
    }
}
