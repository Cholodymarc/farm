package farm;

public enum ExtensionProducts {
    MILK(20000, 7),
    EGGS(50000, 9),
    CHEESE(10000, 16);

    private double extensionCost;
    private double price;
    ExtensionProducts(double extensionCost, double price) {
        this.extensionCost = extensionCost;
        this.price = price;
    }

    public double getExtensionCost() {
        return extensionCost;
    }

    public double getPrice() {
        return price;
    }
}
