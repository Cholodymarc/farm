package farm;

import java.util.List;

public class GranaryManager implements IGranaryManager {
    private Granary granary;

    GranaryManager() {
        this.granary = new Granary();
    }

    public Granary getGranary() {
        return granary;
    }

    public void addProduct(Product product, Granary granary) {
        granary.getProducts().add(product);
    }

    public Product getProductByName(String name){
        return granary.getProductByName(name);
    }

    public List<Product> getProducts(){
        return granary.getProducts();
    }
}
