package farm;

import controllerInput.IScannerController;
import controllerInput.ScannerControllerImpl;
import menu.*;

public class FarmRunner implements IFarmRunner {
    private IScannerController scannerController;
    private GranaryManager granaryManager;
    private Owner owner;
    private MenuList menuList;

    private void loadMenu() {
        menuList.addMenuItem(0, new CloseFarm());
        menuList.addMenuItem(1, new SellProducts());
        menuList.addMenuItem(2, new GenerateProducts());
        menuList.addMenuItem(3, new CheckCashAmount());
        menuList.addMenuItem(4, new ExpandFarm());
    }

    public FarmRunner() {
        this.scannerController = new ScannerControllerImpl();
        this.granaryManager = new GranaryManager();
        this.owner = new Owner(scannerController.loadCashAmount());
        this.menuList = new MenuList();
    }

    public void start() {
        loadBasicGranaryProducts();
        loadMenu();
        chooseNextMenuOption();
    }

    // MENU:
    // 0 - Close Farm
    // 1 - Sell Products
    // 2 - Generate Products
    // 3 - Check Cash Amount
    // 4 - Expand Farm
    private void chooseNextMenuOption() {
        int menuOption = scannerController.pickMenuOption();
        if (menuOption != 0) {
            menuList.getMenuItem(menuOption).performActions(granaryManager, owner, this.scannerController);
            chooseNextMenuOption();
        }
        menuList.getMenuItem(menuOption).performActions(granaryManager, owner, this.scannerController);
    }

    private void addProductToGranary(Product product) {
        granaryManager.addProduct(product, granaryManager.getGranary());
    }

    private void loadBasicGranaryProducts() {
        addProductToGranary(new Product("Wheat", 7, 10));
        addProductToGranary(new Product("Rye", 5, 15));
        addProductToGranary(new Product("Strawberry", 20, 20));
        addProductToGranary(new Product("Carrot", 8, 30));
        addProductToGranary(new Product("Cucumber", 6, 15));
        addProductToGranary(new Product("Watermelon", 9, 6));
        addProductToGranary(new Product("Tomato", 10, 7));
        addProductToGranary(new Product("Lettuce", 3, 9));
        addProductToGranary(new Product("Cabbage", 4, 12));
    }
}
