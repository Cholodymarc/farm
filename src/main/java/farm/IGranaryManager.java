package farm;

import java.util.List;

public interface IGranaryManager {
    Granary getGranary();
    void addProduct(Product product, Granary granary);
    Product getProductByName(String name);
    List<Product> getProducts();
}
