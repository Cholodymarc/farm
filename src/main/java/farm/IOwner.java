package farm;

public interface IOwner {
    double getCashAmount();
    void increaseCashAmount(double amount);
    void decreaseCashAmount(double amount);
}
