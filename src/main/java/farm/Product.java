package farm;

public class Product {
    private final String name;
    private final double price;
    private int amount;

    public Product(String name, double price, int amount) {
        this.name = name;
        this.price = price;
        this.amount = amount;
    }

    public Product(Product product){
        this.name = product.name;
        this.price = product.price;
        this.amount = product.amount;
    }

    public double getPrice() {
        return price;
    }

    public int getAmount() {
        return amount;
    }

    public String getName() {
        return name;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public void decreaseAmount(int amount){
        this.amount -= amount;
    }

    public void produce(int amount) {
        this.amount += amount;
    }
}
