import farm.FarmRunner;
import farm.IFarmRunner;

public class Main {
    public static void main(String[] args) {
        IFarmRunner farmRunner = new FarmRunner();
        farmRunner.start();
    }
}
