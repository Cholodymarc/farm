package menu;

import controllerInput.IScannerController;
import farm.IGranaryManager;
import farm.IOwner;
import seller.SellManager;

public class SellProducts implements Menu {

    @Override
    public void performActions(IGranaryManager granaryManager, IOwner owner, IScannerController scannerController) {
        new SellManager(granaryManager, owner, scannerController);
    }
}
