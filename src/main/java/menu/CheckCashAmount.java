package menu;

import controllerInput.IScannerController;
import farm.IGranaryManager;
import farm.IOwner;

public class CheckCashAmount implements Menu {

    @Override
    public void performActions(IGranaryManager granaryManager, IOwner owner, IScannerController scannerController) {
        System.out.println(owner.getCashAmount());
    }

}
