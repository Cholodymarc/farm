package menu;

import controllerInput.IScannerController;
import farm.Granary;
import farm.IGranaryManager;
import farm.IOwner;

public class CloseFarm implements Menu {

    private void removeGranaryProducts(Granary granary) {
        granary.getProducts().clear();
    }

    @Override
    public void performActions(IGranaryManager granaryManager, IOwner owner, IScannerController scannerController) {
        removeGranaryProducts(granaryManager.getGranary());
    }
}
