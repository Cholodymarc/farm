package menu;

import controllerInput.IScannerController;
import farm.IGranaryManager;
import farm.IOwner;

public interface Menu {
    void performActions(IGranaryManager granaryManager, IOwner owner, IScannerController scannerController);
}
