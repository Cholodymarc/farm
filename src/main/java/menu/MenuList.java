package menu;

import java.util.ArrayList;
import java.util.List;

public class MenuList {
    private List<Menu> menuList;

    public MenuList() {
        this.menuList  = new ArrayList<>();
    }

    public void addMenuItem(int index, Menu menuItem){
        this.menuList.add(index, menuItem);

    }

    public Menu getMenuItem(int index){
        return menuList.get(index);
    }
}
