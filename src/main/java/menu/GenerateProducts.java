package menu;

import controllerInput.IScannerController;
import farm.*;

public class GenerateProducts implements Menu {

    @Override
    public void performActions(IGranaryManager granaryManager, IOwner owner, IScannerController scannerController) {
        supplyProduct(granaryManager.getGranary(), scannerController);
        supplyRunOutProduct(granaryManager.getGranary(), scannerController);
    }

    private void supplyProduct(Granary granary, IScannerController scannerController) {
        String productName = scannerController.pickProductName();

        for (Product product : granary.getProducts()) {
            if (productName.equalsIgnoreCase(product.getName())) {
                product.produce(scannerController.increaseProductAmount());
                break;
            }
        }
    }

    private void supplyRunOutProduct(Granary granary, IScannerController scannerController) {
        for (Product product : granary.getProducts()) {
            if (product.getAmount() == 0) {
                product.produce(scannerController.increaseProductAmount());
            }
        }
    }
}
