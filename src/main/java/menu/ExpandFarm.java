package menu;

import controllerInput.IScannerController;
import farm.ExtensionProducts;
import farm.IGranaryManager;
import farm.IOwner;
import farm.Product;

public class ExpandFarm implements Menu {

    @Override
    public void performActions(IGranaryManager granaryManager, IOwner owner, IScannerController scannerController) {
        expand(scannerController.pickProductName(), granaryManager, owner);
    }

    private void expand(String productName, IGranaryManager granaryManager, IOwner owner){
        ExtensionProducts extensionProduct = ExtensionProducts.valueOf(productName.toUpperCase());
        double extensionCost = extensionProduct.getExtensionCost();
        if(owner.getCashAmount() >= extensionCost){
            granaryManager.addProduct(
                    new Product(productName, extensionProduct.getPrice(),0),
                    granaryManager.getGranary());
            owner.decreaseCashAmount(extensionCost);
        }
    }
}
