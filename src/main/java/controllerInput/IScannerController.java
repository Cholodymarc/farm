package controllerInput;

public interface IScannerController {

    String pickProductName();
    int pickMenuOption();
    int increaseProductAmount();
    double loadCashAmount();
    int pickProductNumber();
}
