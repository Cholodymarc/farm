package controllerInput;

import java.util.Scanner;

public class ScannerControllerImpl implements IScannerController {

    @Override
    public String pickProductName() {
        Scanner scanner = new Scanner(System.in);
        return scanner.next() + scanner.nextLine();
    }

    @Override
    public int pickMenuOption(){
        return new Scanner(System.in).nextInt();
    }

    @Override
    public int increaseProductAmount(){
        return new Scanner(System.in).nextInt();
    }

    @Override
    public double loadCashAmount() {
        return new Scanner(System.in).nextDouble();
    }

    @Override
    public int pickProductNumber() {
        return new Scanner(System.in).nextInt();
    }
}
