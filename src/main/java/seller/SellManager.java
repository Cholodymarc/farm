package seller;

import controllerInput.IScannerController;
import farm.IGranaryManager;
import farm.IOwner;
import farm.Product;
import randomSelection.IRandomSelection;
import randomSelection.RandomSelectionImpl;

import java.util.ArrayList;
import java.util.List;

public class SellManager {
    private List<Order> orders;

    public SellManager(IGranaryManager granaryManager, IOwner owner, IScannerController scannerController) {
        this.orders = new ArrayList<>();
        generateListOfOrders(granaryManager);
//        printOrders();
        if(!pickOrder(scannerController.pickProductNumber(), granaryManager, owner)){
            pickOrder(scannerController.pickProductNumber(), granaryManager, owner);
        }
    }

    private void generateListOfOrders(IGranaryManager granaryManager){
        IRandomSelection randomSelection = new RandomSelectionImpl();
        int numberOfOrders = randomSelection.getRandomInteger(7) + 3;
        for(int i = 0; i < numberOfOrders; i++) {
            orders.add(new Order(granaryManager));
        }
    }

    private boolean pickOrder(int orderIndex, IGranaryManager granaryManager, IOwner owner){
        Order order = orders.get(orderIndex);
        if(order.isEnoughOwnersProductAmount()) {
            for(Product product : order.getDemandedProducts()){
                owner.increaseCashAmount(product.getAmount()*product.getPrice());
                granaryManager.getProductByName(product.getName()).decreaseAmount(product.getAmount());
            }
            orders.remove(orderIndex);
            return true;
        }
        return false;
    }
}
