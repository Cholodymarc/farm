package seller;

public enum WholesalerNames {
    SUNNY, GREEN, FRESH, FARMHOUSE, FRUITERERS, FOREST, TASTY, HEALTHY
}
