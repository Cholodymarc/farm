package seller;

import farm.IGranaryManager;
import farm.Product;
import randomSelection.IRandomSelection;
import randomSelection.RandomSelectionImpl;

import java.util.ArrayList;
import java.util.List;


public class Order {
    private WholesalerNames wholesalerName;
    private List<Product> demandedProducts;
    private boolean enoughOwnersProductAmount;

    Order(IGranaryManager granaryManager) {
        IRandomSelection randomSelection = new RandomSelectionImpl();

        this.wholesalerName = this.getRandomName(randomSelection);
        this.demandedProducts = getRandomDemandedProducts(granaryManager, randomSelection);
        this.enoughOwnersProductAmount = isEnoughOwnersProductAmount(granaryManager);
    }

    private WholesalerNames getRandomName(IRandomSelection randomSelection) {
        return WholesalerNames.values()[randomSelection.getRandomInteger(WholesalerNames.values().length)];
    }

    private List<Product> getRandomDemandedProducts(IGranaryManager granaryManager, IRandomSelection randomSelection) {
        int numberOfProducts = randomSelection.getRandomInteger(5) + 1;
        List<Product> demandedProducts = new ArrayList<>();
        List<Product> granaryProducts = granaryManager.getProducts();

        for (int i = 0; i < numberOfProducts; i++) {
            int randomIndex = randomSelection.getRandomInteger(granaryProducts.size());
            Product randomGranaryProduct = granaryProducts.get(randomIndex);
            Product demandedProduct = new Product(randomGranaryProduct);
            demandedProduct.setAmount(randomSelection.getRandomInteger(20));
            demandedProducts.add(demandedProduct);
        }
        return demandedProducts;
    }

    private boolean isEnoughOwnersProductAmount(IGranaryManager granaryManager){
        for(Product demandedProduct : demandedProducts){
            String demandedProductName = demandedProduct.getName();
            if(demandedProduct.getAmount() > granaryManager.getProductByName(demandedProductName).getAmount()){
                return false;
            }
        }
        return true;
    }

    public WholesalerNames getWholesalerName() {
        return wholesalerName;
    }

    List<Product> getDemandedProducts() {
        return demandedProducts;
    }

    boolean isEnoughOwnersProductAmount() {
        return enoughOwnersProductAmount;
    }
}
