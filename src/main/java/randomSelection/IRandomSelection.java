package randomSelection;

public interface IRandomSelection {
    int getRandomInteger(int bound);
}
