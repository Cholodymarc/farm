package randomSelection;

import java.util.Random;

public class RandomSelectionImpl implements IRandomSelection {

    @Override
    public int getRandomInteger(int bound) {
        return new Random().nextInt(bound);
    }
}
